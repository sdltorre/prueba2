import unittest
from mul import multiply
from mul import add
class MultiplyTestCase(unittest.TestCase):
    def test_multiplication_with_correct_values(self):
        self.assertEqual(multiply(5, 5), 25)
        #si ponemos assertEqual está diciendo que la operación entre paréntesis tiene que ser igual al siguiente número, si no lo es nos saldrá un fallo
    def test_multiplication_with_incorrect_values(self):
        self.assertNotEqual(multiply(5, 5), 24)
    def test_multiplication_with_incorrect_values2(self): #hay que llamar a las funciones diferente
        self.assertEqual(multiply(5, 5), 24)
        #con assertNotEqual decimos que no es igual, si es IGUAL nos dará fallo
    def test_add_with_correct_values(self):
        self.assertEqual(add(5,5), 10)
    

if __name__ == '__main__':
    unittest.main()