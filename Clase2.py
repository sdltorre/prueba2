#un ejemplo de clase para empleados
#def init es el constructor
#python tutor es para que vayas viendo como se ejecuta el programa de python
#diferencia entre clase y objeto, objeto es una copia (Empleado: Pepe, 20000) es un objeto, aqui hay 2 (= instancia)
#clase = 1  en este código
#acordarse de darle al save antes de ejecutarlo
#clase sirve para ordenar las variables y sus valores, así lo estructuramos y no es tan fácil equivocarse


class Empleado:
    def __init__ (self, n , s): #subrayado son cosas de pyhton
        #los self no son variables, son miembro/atributos
        self.nombre = n 
        self.nomina = s 

    def calculo_impuestos(self): #diferencia es que al meter estas funciones los parámetros pertenecen a la clase, son funciones especiales para los empleados porque está dentro de la clase y por eso no tenemos que meter los nombres ni datos como en las funciones antes
        impuestos = self.nomina * 0.30
        return impuestos
    
    def __str__ (self): #hemos transformado las funciones en métodos
        return("El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax =self.calculo_impuestos()))
    
# .2f es para poner dos decimales
#si solo hay un hueco podemos ponerlo vacio, si hay varios hay que definir qué queremos que salga en cada uno (tiene que salir en azul)
#mucho mejor usar este print


empleadoPepe = Empleado ('Pepe', 20000)
empleadaAna = Empleado ('Ana', 30000)
#print("El nombre del empleado es {}".format(empleadoPepe.nombre))

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()
print(empleadoPepe) #imprime lo mismo de antes
print(empleadaAna)

#empleadaAna.imprime()
#empleadoPepe.imprime()
print("Los impuestos a pagar en total son {:.2f} euros".format(total))

#lo del format mete lo siguiente(empleadoPepe la característica nombre) en el hueco
#parecido a lo de self.nombre. Ponemos una instancia(objeto) y un punto y después uno de los datos de la clase para sacralo
#metodos(orientación a objetos): 'instancia, quiero que me hagas esto'
#instancia.método() = ejecuta la funcion especial dentro de la clase con la instancia que le hayamos puesto
#self comodín para decir yo, para decir quién  somos lo ponemos delante = INSTANCIA