class Empleado: 
    def __init__(self, nombre, salario, tasa, edad, antigüedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__edad= edad 
        self.__antigüedad = antigüedad
        #self es para ver a donde esta apuntando

    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre, tax=self.__impuestos))
        return self.__impuestos 
    
    def ImprimirEdad(self):
        print('Edad de ' + self.__nombre + ':' + str(self.__edad))

    def AhorroImpuestos(self):
        if (self.__antigüedad > 1):
            print ("Antigüedad del empleado mayor que un año ")

    
def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))


emp1 = Empleado("Pepe", 20000, 0.35, 50, 10)
emp2 = Empleado("Ana", 30000, 0.30, 60, 0.2)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 30, 10), Empleado("Luisa", 25000, 0.15, 42, 8)]
total = 0
for emp in empleados:
    emp.ImprimirEdad()
    emp.AhorroImpuestos()
    total += emp.CalculoImpuestos()

displayCost(total)

