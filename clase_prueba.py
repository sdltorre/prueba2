class Rectángulo:
    """"Ejemplo de clase para un cuadrado""" #comentario a modo de subtítulo o documentación, si paso el cursor por encima de cuadrado lo pone
    def __init__(self, b,a):
        self.base = b
        self.altura = a
    
    def calculo_perimetro(self):
        perimetro= self.base * 2 + self.altura * 2
        return perimetro
    
    def calculo_area(self):
        area= self.base * self.altura
        return area
    
    def __str__ (self):
        return("El rectángulo de base {base} y altura de {altura} tiene un perímetro de  {p:.2f} y un área de {a:.2f}".format(base=self.base, altura= self.altura,  p =self.calculo_perimetro(), a = self.calculo_area()))

rectángulo1 = Rectángulo (20, 12)
rectángulo2 = Rectángulo (1.46, 3)
print(rectángulo1)
print(rectángulo2)
print (rectángulo1.base) #asi imprime cualquiera de los atributos de un objeto
#podemos hacerlo también para un cuadrado, eliminando la  variable de base y poner un único lado, o poniendo los dos lados iguales en este programa
#si queremos imprimir alguno de los dos datos tenemos que convertirlo a string poniendo str(self.base)